use std::rand;
use std::io;
use std::cmp::Ordering;

fn cmp(a: u32, b: u32) -> Ordering {
    if a > b {
        Ordering::Greater
    } else if a < b {
        Ordering::Less
    } else {
        Ordering::Equal
    }
}

fn main() {
    let secret_number = rand::random::<u32>() % 100;
    let mut guess_count = 0;

    println!("Guess a number from 1 to 100");

    loop {
        let input = io::stdin()
                        .read_line()
                        .ok()
                        .expect("Error getting input");

        let input_num_opt: Option<u32> = input.trim().parse();

        let num = match input_num_opt {
            Some(num) => num,
            None => {
                println!("Not a number: {}", input);
                return;
            }
        };

        guess_count = guess_count + 1;

        match cmp(num, secret_number) {
            Ordering::Greater => println!("Nah, make it lower"),
            Ordering::Less => println!("Nah, make it higher"),
            Ordering::Equal => {
                println!("Holy shit you win at {} tries", guess_count);
                break;
            },
        }
    }
}
